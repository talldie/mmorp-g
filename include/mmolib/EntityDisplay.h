#ifndef MMO_ENTITYDISPLAY_H
#define MMO_ENTITYDISPLAY_H

/// libraries ///////////////////////////////////////
#include "Entity.h"
#include "Target.h"
#include "AssetPool.h"
#include "ResourceManager.h"

#include <raylib.h>

#include <vector>
#include <cstdint>
/// forward declarations ///////////////////////////

namespace MMO{

    /// forward declarations //////////////////////

    /// type declarations //////////////////////////
    class EntityDisplay : public Target {

    public:

        EntityDisplay(Entity& entity);

        virtual ~EntityDisplay();

        float &x();

	float &y();

	float &z();

        virtual void update(float dt);

        const float &x() const;

        const float &y() const;

        const float &z() const;

        std::vector<AssetEntry<Model>> &getModels();

        const std::vector<AssetEntry<Model>> &getModels() const;

    protected:

        Entity& entity;

        std::vector<AssetEntry<Model>> models;

        virtual void getEntityModels();

    };

}  // namespace MMO

#endif //MMO_ENTITYDISPLAY_H