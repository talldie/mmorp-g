#include "../ResourceManager.h"

namespace MMO {

    AssetEntry<Texture2D> getTexture(char* fName)
    {
        size_t textureHash = nHash(strlen(fName), fName);  // temporary will be altered when we have (n)strings
        if (texturePool.contains(textureHash)) {
            return texturePool.find(textureHash);
        } else {
            Texture2D texture = LoadTexture(fName);
            if (texture.id != 0)  //if load did not fail
                return texturePool.insert(textureHash, texture);
            else
                return AssetEntry<Texture2D> { texture, 0, 0 };  // returned a failed entry
        }
    }

    void releaseTexture(AssetEntry<Texture2D> texture)
    {
        texturePool.remove(texture.hash);
    }

    AssetEntry<Model> getModel(char* fName)
    {
        size_t modelHash = nHash(strlen(fName), fName);  // temporary will be altered when we have (n)strings
        if (modelPool.contains(modelHash)) {
            return modelPool.find(modelHash);
        } else {
            Model model = LoadModel(fName);

            // model loading can't fail (one of the lovely inconsistencies of raylib) instead a failed model generates a cube mesh which still has to be released
            return modelPool.insert(modelHash, model);
        }
    }

    void releaseModel(AssetEntry<Model> model)
    {
        modelPool.remove(model.hash);
    }

    size_t nHash(size_t n, char* fPath)
    {
        size_t hash = 5381;
        while (n--)  // for ever char of the string
            hash = ((hash << 5) + hash) + *fPath++;  // modify the hash
        return hash;
    }

}   // namespace MMO