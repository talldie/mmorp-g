#include "../EntityDisplay.h"

namespace MMO {

    EntityDisplay::EntityDisplay(Entity& entity)
    : entity(entity)
    {
        getEntityModels();  // need a safe way to do this
    }

    void EntityDisplay::update(float dt)
    {
        // unimplemented function
    }

    EntityDisplay::~EntityDisplay()
    {
        for (auto en : models) {
            releaseModel(en);
        }
    }

    float& EntityDisplay::x()
    {
        return entity.x;
    }

    float& EntityDisplay::y()
    {
        return entity.y;
    }

    float& EntityDisplay::z()
    {
        return entity.z;
    }

    const float& EntityDisplay::x() const
    {
        return entity.x;
    }

    const float& EntityDisplay::y() const
    {
        return entity.y;
    }

    const float& EntityDisplay::z() const
    {
        return entity.z;
    }

    std::vector<AssetEntry<Model>>& EntityDisplay::getModels()
    {
        return models;
    }

    const std::vector<AssetEntry<Model>>& EntityDisplay::getModels() const
    {
        return models;
    }

    void EntityDisplay::getEntityModels()
    {
        switch (entity.head) {
            case HEAD_MESH_TEST:
            default:
                models.push_back(getModel("../assets/entities/character_head.gltf"));
                break;
        }

        switch (entity.body) {
            case BODY_MESH_TEST:
            default:
                models.push_back(getModel("../assets/entities/character_torso.gltf"));
                break;
        }

        switch (entity.hands) {
            case HAND_MESH_TEST:
            default:
                models.push_back(getModel("../assets/entities/character_arms.gltf"));
                break;
        }

        switch (entity.legs) {
            case LEG_MESH_TEST:
            default:
                models.push_back(getModel("../assets/entities/character_legs.gltf"));
                break;
        }
    }

}   // namespace MMO