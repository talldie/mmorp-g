#ifndef MMO_TARGET_H
#define MMO_TARGET_H

/// libraries ///////////////////////////////////////

/// forward declarations ///////////////////////////

namespace MMO {

    /// forward declarations //////////////////////

    /// type declarations //////////////////////////

    /***
     * update interface for objects that need periodically updating allowing them to be placed in update target lists
     */
    class Target {

    public:

        virtual ~Target() = default;

        /***
         * Update the target
         * @param dt time since last update
         */
        virtual void update(float dt) = 0;

    };
}   // namespace MMO

#endif  //MMO_TARGET_H
