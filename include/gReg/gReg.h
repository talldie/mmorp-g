#ifndef GREG_H
#define GREG_H

#include <tixi.h>
#include <gLog/gLog.h>
#include <cstdint>
#include <string>


// node/attr declarations
static constexpr const char* kRegName      = "reg.xml";
static constexpr const char* kRegRootName  = "greg_root";
static constexpr const char* kRegRootNode  = "/greg_root";

bool greg_createRegFile();

bool greg_AddIntegerValue(const char * eleName, int val);

bool greg_AddDoubleValue(const char * eleName, double val);

bool greg_saveAndExit();

static TixiDocumentHandle sRegHandle = -1;

#endif  // GREG_H
