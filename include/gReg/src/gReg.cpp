#include "../gReg.h"

bool greg_createRegFile()
{
	ReturnCode theErr;
	theErr = tixiCreateDocument(kRegRootName, &sRegHandle);
	GLOG_REQUIRE(theErr == SUCCESS, "[GREG]: failed to create regFile: could not create document");

	theErr = tixiSaveDocument(sRegHandle, kRegName);
	GLOG_REQUIRE(theErr == SUCCESS, "[GREG]: failed to create regFile: could not save document");

	theErr = tixiCloseDocument(sRegHandle);
	GLOG_REQUIRE(theErr == SUCCESS, "[GREG]: failed to create regFile: could not close document");

	theErr = tixiOpenDocument(kRegName, &sRegHandle);
	GLOG_REQUIRE(theErr == SUCCESS, "[GREG]: failed to create regFile: could not open document");

	GLOG_REQUIRE(theErr == SUCCESS, "[GREG]: failed to create regFile: could not add root to document");
	return theErr == SUCCESS;
}

bool greg_AddIntegerValue(const char * eleName, int val)
{
	ReturnCode theErr;
	theErr = tixiAddIntegerElement(sRegHandle, kRegRootNode, eleName, val,"%i");
	tixiAddTextAttribute(sRegHandle, ((std::string) kRegRootNode).append("/").append(eleName).c_str(), "type", "Integer" );
	return theErr == SUCCESS;
}

bool greg_AddDoubleValue(const char * eleName, double val)
{
	ReturnCode theErr;
	theErr = tixiAddDoubleElement(sRegHandle, kRegRootNode, eleName, val,"%f");
	tixiAddTextAttribute(sRegHandle, ((std::string) kRegRootNode).append("/").append(eleName).c_str(), "type", "Double" );
	return theErr == SUCCESS;
}

bool greg_saveAndExit()
{
	ReturnCode theErr;
	theErr = tixiSaveDocument(sRegHandle, kRegName);
	GLOG_REQUIRE(theErr == SUCCESS, "[GREG]: failed to create regFile: could not save document");

	theErr = tixiCloseDocument(sRegHandle);
	GLOG_REQUIRE(theErr == SUCCESS, "[GREG]: failed to create regFile: could not close document");

	return true;
}