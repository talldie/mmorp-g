#!/bin/bash

# util.sh
#
# useful functions for use in writing hooks and working in the repository.

check_format_single_file() {
    file=$1
    clang-format --dry-run --Werror "$file"
}

build() {
    ninja -C build
}


get_modified_c_files() {
    declare -a C_EXTENSIONS=(
        "hpp" "h" "cpp" "c" "cc"
    )

    extension_regex=""
    prefix=""
    for ext in "${C_EXTENSIONS[@]}"; do
        extension_regex="$extension_regex$prefix.*\.$ext\$"
        prefix="|"
    done

    files=$(git diff --cached --name-only | grep -E "$extension_regex")
    echo "$files"
}

# Currently only formats C/C++ header/source files.
check_format() {
    files=$(get_modified_c_files)

    unformatted=""
    while IFS= read -r line; do
        echo "Verifying format for... $line"
        if ! check_format_single_file "$line" 2>/dev/null ; then
            if [[ -z $unformatted ]]; then
                unformatted=$line
            else
                unformatted="$line\n$unformatted"
            fi
        fi
    done <<< "$files"

    if [[ -n $unformatted ]]; then
        echo "Unformatted files"
        echo "$unformatted" | sort -r 
    fi
}

format() {
    files=$(get_modified_c_files)
    while IFS= read -r line; do
        echo "Formatting... $line"
        clang-format --style=file -i "$line"
    done <<< "$files"
}
