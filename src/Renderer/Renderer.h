#ifndef MMORP_G_RENDERER_H
#define MMORP_G_RENDERER_H

#include <raylib.h>
#include <raymath.h>
#include <mmolib/Level.h>
#include <mmolib/Target.h>
#include <mmolib/Entity.h>
#include <mmolib/EntityDisplay.h>

class Renderer : public MMO::Target
{

	Camera currentCamera;
	MMO::Level currentLevel;

	std::vector<MMO::EntityDisplay *> entities;

public:

	Renderer();

	virtual ~Renderer();

	void presentWindow();

	void hideWindow();

	/***
	 * Returns if the window has been requested for close
	 * @return true if close is requested
	 */
	bool closeRequested();

	/***
	 * Get the current Render camera
	 * @return Current camera
	 */
	Camera *getCurrentCamera();

	/***
	 * Loads a level to be rendered
	 * @param level Level to be loaded
	 */
	void loadLevel(MMO::Level level);

	/***
	 * adds an EntityDisplay to the list of render targets
	 * @param entityDisplay Display to render
	 */
	void addEntityTarget(MMO::EntityDisplay *entityDisplay);

	/***
	 * Update and render targets
	 * @param dt Time since last frame
	 */
	void update(float dt) override;

protected:

	void renderTargets();

	void renderLevel();

	void renderEntities();

	void startRenderpass();

	void endRenderpass();

private:

	void initCamera();

};

#endif  //MMORP_G_RENDERER_H
