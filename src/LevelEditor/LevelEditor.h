#ifndef MMORP_G_LEVELEDITOR_H
#define MMORP_G_LEVELEDITOR_H



#include <raylib.h>
#include <raymath.h>
#include <mmolib/Target.h>
#include <mmolib/Level.h>

class LevelEditor : public MMO::Target {

    Camera *camera;

    MMO::Level level;

    float brushStrength;
    float brushRadius;

public:

    LevelEditor(MMO::Level level, Camera *camera);

    virtual ~LevelEditor();

    void update(float dt) override;

    /***
     * pulls vertices of given mesh
     * @param mesh Mesh to pull
     * @param pos  Position to pull from
     * @param Direction direction to pull to
     * @param str Pull strength
     * @param range Range of pull
     */
    void pullLevelMesh(Vector3 pos, Vector3 direction, float str, float range);

    /***
     * pushes vertices of given mesh
     * @param mesh Mesh to push
     * @param pos Position to push from
     * @param Direction direction to push to
     * @param str Push strength
     * @param range Range of push
     */
    void pushLevelMesh(Vector3 pos, Vector3 direction, float str, float range);
};

#endif  //MMORP_G_LEVELEDITOR_H
