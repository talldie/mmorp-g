#include <cstdio>
#include "LevelEditor.h"

LevelEditor::LevelEditor(MMO::Level level, Camera *camera)
{
    this->level = level;
    this->camera = camera;

    brushStrength = 3;
    brushRadius   = 3;
}

LevelEditor::~LevelEditor() = default;

void LevelEditor::update(float dt)
{
    Ray ray = GetMouseRay(GetMousePosition(), *camera);

    RayHitInfo col = GetCollisionRayMesh(ray, level.getMesh(), level.getModel().transform);

    if(col.hit){

        if(IsMouseButtonDown(MOUSE_LEFT_BUTTON) || IsMouseButtonDown(MOUSE_RIGHT_BUTTON))
        {
            if (IsMouseButtonDown(MOUSE_LEFT_BUTTON))
                pushLevelMesh(col.position, col.normal, brushStrength * dt, brushRadius);

            if (IsMouseButtonDown(MOUSE_RIGHT_BUTTON))
                pullLevelMesh(col.position, col.normal, brushStrength * dt, brushRadius);

            //TODO: vertex merge step

            level.updateLevelMesh();
        }
    }
}

//TODO: add vertices to preserve geometry

void LevelEditor::pullLevelMesh(Vector3 pos, Vector3 direction, float str, float range)
{
    std::vector<int> verts = level.getVerticesWithinRange(pos, range);
    const Mesh& mesh = level.getMesh();
    for(int i = 0; i < verts.size(); i+=3 )
    {
        mesh.vertices[verts[i + 0]] += direction.x * str;
        mesh.vertices[verts[i + 1]] += direction.y * str;
        mesh.vertices[verts[i + 2]] += direction.z * str;
    }
}

void LevelEditor::pushLevelMesh(Vector3 pos, Vector3 direction, float str, float range)
{
    std::vector<int> verts = level.getVerticesWithinRange(pos, range);
    const Mesh& mesh = level.getMesh();
    for(int i = 0; i < verts.size(); i+=3 )
    {
        mesh.vertices[verts[i + 0]] -= direction.x * str;
        mesh.vertices[verts[i + 1]] -= direction.y * str;
        mesh.vertices[verts[i + 2]] -= direction.z * str;
    }
}