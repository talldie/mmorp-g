#include "dispatch.h"
#include <cstddef>

// How many request types we can support.
constexpr int32_t MAX_NUM_ENDPOINTS = 32;

// The request dispatch function takes in two arguments:
// 1. The state of the server
// 2. A pointer to a representation of the handled request
// 3. A pointer to a representation of the responses, which we'll populate (out
// parameter).
//
// We'll populate the response object, and we may need to update some
// server-side state , so both must be passed via nonconst pointer.
typedef void (*DispatchFn)(
  ServerInfo* req, const character::Request* data, character::Response* resp);

// A mapping between int and a function pointer that will process the request.
typedef struct{ DispatchFn request_type[MAX_NUM_ENDPOINTS]; } DispatchVector;

// Response Handlers.
void CreateCharacter(ServerInfo* req, const character::Request* data, character::Response* resp);

constexpr DispatchVector REQUEST_HANDLER_VECTOR = {
  CreateCharacter,
};

void DispatchHandler(ServerInfo& si, const character::Request& client_request, character::Response& resp)
{
  int index = int(client_request.type());
  if(index < 0 || index >= MAX_NUM_ENDPOINTS)
  {
    fprintf(stderr, "[ERROR] %s Index out of range: %d\n", __func__, index);
  }
  DispatchFn handler = REQUEST_HANDLER_VECTOR.request_type[index];
  handler(&si, &client_request, &resp);
}


size_t GetNextClientId(const ServerInfo* si) {
  return si->client_entities.characters_size() + 1;
}

character::BasicCharacter GenerateCharacter(const char* name, const ServerInfo* si) {
  size_t player_id = GetNextClientId(si);

  character::Position character;
  character.set_x(1.0 * player_id);
  character.set_y(0.0);
  character.set_z(0.0);

  character::Position camera_position;
  camera_position.set_x(18.83 + 1.0 * player_id);
  camera_position.set_y(12.89);
  camera_position.set_z(12.0);

  character::BasicCharacter c;
  c.set_name(name);
  c.set_player_id(player_id);
  c.mutable_pos()->CopyFrom(character);

  character::Camera* camera = c.mutable_camera();
  camera->mutable_position()->CopyFrom(camera_position);
  camera->mutable_target()->CopyFrom(character);

  return c;
}

void CreateCharacter(
  ServerInfo* si, const character::Request* req, character::Response* resp)
{
  // TODO: Error checking for protobuf oneof field? 
  printf("Responding to creating a new character!\n");

  const character::CreateCharacterRequest request = req->create_char();
  character::BasicCharacter new_char = GenerateCharacter(request.name().c_str(), si);

  character::CreateCharacterResponse response;
  response.mutable_new_character()->CopyFrom(new_char);

  // Set the oneof field in the response.
  resp->set_type(req->type());
  resp->mutable_create_char()->CopyFrom(response);

  character::BasicCharacter* added_char = si->client_entities.add_characters();
  added_char->CopyFrom(new_char);

  printf("Done creating a new character!\n");
}
