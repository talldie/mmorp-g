#include <stdio.h> // perror, printf
#include <stdlib.h> // exit, atoi
#include <unistd.h> // read, write, close
#include <arpa/inet.h> // sockaddr_in, AF_INET, SOCK_STREAM, INADDR_ANY, socket etc...
#include <string.h> // memset
#include <fcntl.h>

#include "basic_character.pb.h"

#include "dispatch.h"
#include "server_info.h"

void ReceiveMessage(const ServerInfo& si,
                    character::Request& client_request,
                    struct sockaddr_in& client,
                    socklen_t& message_len);
void SendResponse(const ServerInfo& si,
                  character::Response& resp,
                  const struct sockaddr_in& client,
                  socklen_t& addrlen);
void InitializeServer(ServerInfo& si, short port);

constexpr short PORT_NUM = 8543;

int main(int argc, char* argv[]) {
  ServerInfo server_info;
  printf("Initializing Server!\n");
  InitializeServer(server_info, PORT_NUM);

  printf("Running main loop!\n");

  while(true)
  {
    printf("Beginning processing loop.\n");

    // Receive a packet from a client (store in client_request).
    struct sockaddr_in client;
    character::Request client_request;
    socklen_t client_length;
    // client, client_request, and client_length are all out-parameters.
    printf("Receiving a Message\n");
    ReceiveMessage(server_info, client_request, client, client_length);

    // Call the handler.
    character::Response server_response;
    DispatchHandler(server_info, client_request, server_response);

    // Send response.
    printf("Sending a Response\n");
    SendResponse(server_info,server_response, client, client_length);
    printf("End Processing Loop. Sleeping for three seconds.\n");
    sleep(3);
  }

  return 0;
}

/// ReceiveMessage
/// Given a ServerInfo, waits for a message to be received on the server's
/// socket file descriptor (blocking).
///
/// Then, it decodes the message sent based on the first four bytes received
/// (which indicate what the type of the message was), and populates the out
/// parameter `client_request`
constexpr size_t BUFFER_SIZE = 2048;
constexpr size_t UDP_NO_FLAGS = 0;
void ReceiveMessage(const ServerInfo& si,
                    character::Request& client_request,
                    struct sockaddr_in& client,
                    socklen_t& message_len)
{
  // buffer to hold our received message.
  static char message_buffer[BUFFER_SIZE];
  message_len = sizeof(client);

  size_t bytes_received = recvfrom(
    si.server_socket, message_buffer, BUFFER_SIZE, UDP_NO_FLAGS,
    (struct sockaddr*) &client, &message_len);

  if(bytes_received < 0) {
    fprintf(stderr, "Error in receiving packet. Errno code: %d\n", errno);
    exit(1);
  }

  // Parse the buffer.
  bool result = client_request.ParseFromArray(message_buffer, bytes_received);
  if(!result)
  {
    fprintf(stderr, "Error in decoding packet");
    exit(2);
  }

  // Otherwise, just print a short diagnostic message.
  std::string printable_rt = character::RequestType_Name(client_request.type());
  printf("[INFO] Request Type: %s\n",printable_rt.c_str());
}

/// SendResponse
/// Given a Response object and a client, encodes the Response into a byte array
/// and sends it to the client using the UDP function sendto.
void SendResponse(const ServerInfo& si, character::Response& resp, const struct sockaddr_in& client, socklen_t& addrlen)
{
  static char message_buffer[BUFFER_SIZE];

  size_t num_serialized_bytes = resp.ByteSizeLong();
  bool success = resp.SerializeToArray(message_buffer, num_serialized_bytes);
  if(!success)
  {
    perror("Serialization Failure... Exiting.\n");
    exit(3);
  }

  ssize_t result = sendto(si.server_socket, message_buffer, num_serialized_bytes,
                          UDP_NO_FLAGS,
                          (struct sockaddr*)&client, addrlen);

  if(result < 0)
  {
    perror("Error in sending to client. Exiting.\n");
    exit(4);
  }
}

void InitializeServer(ServerInfo& si, short port)
{
  ServerSocket server_fd = socket(AF_INET, SOCK_DGRAM, 0);
  if(server_fd < 0) {
    perror("Cannot create socket.");
    exit(1);
  }

  struct sockaddr_in server;
  server.sin_family = AF_INET;

  // The socket accepts connections to all the IPs of the machine.
  server.sin_addr.s_addr = inet_addr("0.0.0.0"); // bind to all available interfaces.
  server.sin_port = htons(port);

  size_t len = sizeof(server);
  if (bind(server_fd, (struct sockaddr*) &server, len) < 0) {
    perror("Cannot bind socket.");
    exit(2);
  }

  // Note that we don't call listen() since that's only for SOCK_STREAM or
  // SOCK_SEQPACKET.
  si.server = server;
  si.client_entities = character::CharacterList();
  si.server_socket = server_fd;
}
