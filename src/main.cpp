


#include "Renderer/Renderer.h"
#include "LevelEditor/LevelEditor.h"
#include "CameraController/EntityCameraController.h"
#include "EntityController/EntityController.h"

#include <vector>
#include <stdio.h>
#include <mmolib/ResourceManager.h>
#include <mmolib/Entity.h>
#include <mmolib/EntityDisplay.h>
#include <mmolib/Level.h>
//TODO: Eventually integrate with client-server demo


int main(void)
{

	Renderer renderer;

	std::vector<MMO::Target *> renderTargets{};
	std::vector<MMO::Target *> logicTargets{};

	logicTargets.push_back(&renderer);

	// TODO: game state machine for loading

	// demo platform
	MMO::Level level;
	level.setMesh(GenMeshPlane(80, 80, 80, 80));
	MMO::AssetEntry<Texture2D> tex = MMO::getTexture("../assets/test.png");
	MMO::AssetEntry<Texture2D> tex2 = MMO::getTexture("../assets/testNorm.png");
	level.addTextureToMap(tex, MATERIAL_MAP_DIFFUSE);
	level.addTextureToMap(tex2, MATERIAL_MAP_NORMAL); // this normal won't actually do anything
	                                                          // as we have no light pass configured to use it

	MMO::Entity entity{};
	entity.y = 5.0f;
	MMO::EntityDisplay entityDisplay(entity);
	renderer.addEntityTarget(&entityDisplay);

	renderer.loadLevel(level);

	LevelEditor levelEditor(level, renderer.getCurrentCamera());

	EntityCameraController ecc(entity, renderer.getCurrentCamera());
	logicTargets.push_back(&ecc);

	EntityController ec(entity, ecc);
	ec.addCollisionModel(&level.getModel());
	logicTargets.push_back(&ec);

	renderer.presentWindow();

	float dt = 0;

	while (!renderer.closeRequested())
	{

		dt = GetFrameTime();

		for (auto *tar: renderTargets)
		{
			tar->update(dt);
		}

		for (auto *tar: logicTargets)
		{
			tar->update(dt);
		}

	}
	MMO::releaseTexture(tex);
	MMO::releaseTexture(tex2);

	return 0;
}
