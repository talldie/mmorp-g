#ifndef MMORP_G_ENTITY_CAMERA_CONTROLLER_H
#define MMORP_G_ENTITY_CAMERA_CONTROLLER_H

#include <mmolib/Entity.h>

#include "CameraController.h"

#include <raylib.h>
#include <raymath.h>

class EntityCameraController : public CameraController
{

public:

    EntityCameraController(MMO::Entity& playerEntity, Camera3D* camera);

    virtual ~EntityCameraController();

    virtual void update(float dt) override;

protected:

    MMO::Entity& playerEntity;
    float distance;
    float angle;

};

#endif  // MMORP_G_ENTITY_CAMERA_CONTROLLER_H