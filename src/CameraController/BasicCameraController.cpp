#include "BasicCameraController.h"

BasicCameraController::BasicCameraController(Camera *camera)
{
    this->camera = camera;
    speed = 5;
}

BasicCameraController::~BasicCameraController() = default;

void BasicCameraController::update(float dt) {

    if(IsKeyDown(KEY_W))
        camera->position.z += speed * dt;

    if(IsKeyDown(KEY_S))
        camera->position.z -= speed * dt;

    if(IsKeyDown(KEY_A))
        camera->position.x -= speed * dt;

    if(IsKeyDown(KEY_D))
        camera->position.x += speed * dt;

    if(IsKeyDown(KEY_Q))
        camera->position.y -= speed * dt;

    if(IsKeyDown(KEY_E))
        camera->position.y += speed * dt;
}

