#ifndef MMORP_G_BASICCAMERACONTROLLER_H
#define MMORP_G_BASICCAMERACONTROLLER_H

#include "CameraController.h"

#include <raylib.h>

class FreeCameraController : public CameraController{

public:

    FreeCameraController(Camera* camera);

    virtual ~FreeCameraController();

    void update(float dt) override;
protected:

    float speed;

};

#endif  //MMORP_G_BASICCAMERACONTROLLER_H
