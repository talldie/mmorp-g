#ifndef MMORP_G_BASICCAMERACONTROLLER_H
#define MMORP_G_BASICCAMERACONTROLLER_H

#include <mmolib/Target.h>

#include <raylib.h>

class BasicCameraController : public MMO::Target{

    Camera* camera;

    float speed;

public:

    BasicCameraController(Camera* camera);

    virtual ~BasicCameraController();

    void update(float dt) override;

};

#endif  //MMORP_G_BASICCAMERACONTROLLER_H
