#include <cstdio>
#include "EntityCameraController.h"

EntityCameraController::EntityCameraController(MMO::Entity &playerEntity, Camera* camera):
playerEntity(playerEntity)
{
    this->camera = camera;

    this->angle = 0;
    this->distance = 20;
}

EntityCameraController::~EntityCameraController() = default;

void EntityCameraController::update(float dt)
{
    /*
    camera.position = (Vector3) { 30.0f, 30.0f, 30.0f };
    currentCamera.up = (Vector3) { 0.0f, 1.0f, 0.0f };
    currentCamera.fovy = 45.0f;
    */

    //TODO: unit vector from polar coords

    if(IsMouseButtonPressed(MOUSE_RIGHT_BUTTON))
    {
        rightClickStartPos = GetMousePosition();
    }

    if(IsMouseButtonDown(MOUSE_RIGHT_BUTTON))
    {
        angle += (rightClickStartPos.x - GetMousePosition().x);
        rightClickStartPos = GetMousePosition();
    }


    camera->position = { playerEntity.x - (distance * cos(angle * DEG2RAD)), 20, playerEntity.z - (distance * sin(angle * DEG2RAD)) };
    camera->target = (Vector3) { playerEntity.x, playerEntity.y, playerEntity.z };

}